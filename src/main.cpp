#include <cantino.h>

#include "Instruction.h"
#include "PushButton.h"
#include "RobotArm.h"
#include "Queue.h"

using namespace cantino;
using namespace PedZed::ArduinoUtils;
using namespace PedZed::RobotArm;

const int BASE_LEFT = -85;
const int BASE_CENTER = 3;
const int BASE_RIGHT = 90;
const int BASE_DEFAULT = BASE_CENTER;

const int UPPERARM_FORWARD = 60;
const int UPPERARM_CENTER = -18;
const int UPPERARM_BACKWARD = -45;
const int UPPERARM_DEFAULT = UPPERARM_CENTER;

const int FOREARM_FORWARD = 45;
const int FOREARM_CENTER = -56;
const int FOREARM_BACKWARD = -90;
const int FOREARM_DEFAULT = FOREARM_CENTER;

const int H_WRIST_LEFT = -90;
const int H_WRIST_CENTER = 10;
const int H_WRIST_RIGHT = 90;
const int H_WRIST_DEFAULT = H_WRIST_CENTER;

const int V_WRIST_FORWARD = 45;
const int V_WRIST_CENTER = -79;
const int V_WRIST_BACKWARD = -90;
const int V_WRIST_DEFAULT = V_WRIST_CENTER;

const int CLAW_CLOSED = 90;
const int CLAW_OPENED = 0;

const Instruction CENTER_INSTRUCTION(
    0, BASE_CENTER, UPPERARM_CENTER, FOREARM_CENTER, V_WRIST_CENTER, H_WRIST_CENTER, CLAW_CLOSED
);

const int DEMO_INSTRUCTIONS[][7] = {
    { 500, BASE_CENTER, UPPERARM_CENTER + 40, FOREARM_CENTER, V_WRIST_CENTER, H_WRIST_CENTER, CLAW_CLOSED },
    { 500, BASE_CENTER, UPPERARM_FORWARD, FOREARM_CENTER, V_WRIST_CENTER, H_WRIST_CENTER, CLAW_OPENED },
    { 500, BASE_CENTER, UPPERARM_FORWARD, FOREARM_CENTER + 20, V_WRIST_CENTER, H_WRIST_CENTER, CLAW_OPENED },
    { 500, BASE_CENTER, UPPERARM_FORWARD, FOREARM_CENTER + 20, V_WRIST_CENTER, H_WRIST_CENTER, CLAW_OPENED },
    { 1500, BASE_CENTER, UPPERARM_FORWARD, FOREARM_CENTER + 20, V_WRIST_CENTER, H_WRIST_CENTER, CLAW_CLOSED - 34 },
    { 500, BASE_CENTER, UPPERARM_CENTER + 40, FOREARM_CENTER + 20, V_WRIST_CENTER + 20, H_WRIST_CENTER, CLAW_CLOSED - 34 },
    { 500, BASE_LEFT, UPPERARM_CENTER + 40, FOREARM_CENTER + 20, V_WRIST_CENTER + 20, H_WRIST_CENTER, CLAW_CLOSED - 34 },
    { 2000, BASE_LEFT, UPPERARM_CENTER + 40, FOREARM_CENTER + 20, V_WRIST_CENTER + 20, H_WRIST_CENTER, CLAW_OPENED },
    { 500, BASE_CENTER, UPPERARM_CENTER + 40, FOREARM_CENTER + 20, V_WRIST_CENTER + 20, H_WRIST_CENTER, CLAW_CLOSED },
    { 500, BASE_CENTER, UPPERARM_CENTER, FOREARM_CENTER, V_WRIST_CENTER, H_WRIST_CENTER, CLAW_CLOSED },
};

int main()
{
    cout << "Program started." << endl;

    PushButton disableButton(2);
    PushButton enableButton(3);
    PushButton centerButton(4);
    PushButton demoButton(5);
    RobotArm robotArm;

    while (true) {
        if (enableButton.isReleased()) {
            cout << "[main] enableButton.isReleased()" << endl;
            robotArm.enable();
        }

        if (disableButton.isReleased()) {
            cout << "[main] disableButton.isReleased()" << endl;
            robotArm.disable();
        }

        if (centerButton.isReleased()) {
            cout << "[main] centerButton.isReleased()" << endl;

            Queue<Instruction> instructionQueue = Queue<Instruction>(1);
            instructionQueue.push(CENTER_INSTRUCTION);
            robotArm.setInstructions(&instructionQueue);
        }

        if (demoButton.isReleased()) {
            cout << "[main] demoButton.isReleased()" << endl;

            Queue<Instruction> demoInstructionQueue = Queue<Instruction>(10);

            int instructionCount = sizeof(DEMO_INSTRUCTIONS) / sizeof(DEMO_INSTRUCTIONS[0]);

            for (int instructionIndex = 0; instructionIndex < instructionCount; instructionIndex++) {
                demoInstructionQueue.push(Instruction(
                    DEMO_INSTRUCTIONS[instructionIndex][0],
                    DEMO_INSTRUCTIONS[instructionIndex][1],
                    DEMO_INSTRUCTIONS[instructionIndex][2],
                    DEMO_INSTRUCTIONS[instructionIndex][3],
                    DEMO_INSTRUCTIONS[instructionIndex][4],
                    DEMO_INSTRUCTIONS[instructionIndex][5],
                    DEMO_INSTRUCTIONS[instructionIndex][6]
                ));
            }

            robotArm.setInstructions(&demoInstructionQueue);
        }

        enableButton.update();
        disableButton.update();
        centerButton.update();
        demoButton.update();
        robotArm.update();
    }

    return 0;
}

#include "RobotArmComponent.h"

using PedZed::RobotArm::RobotArmComponent;

RobotArmComponent::RobotArmComponent(uint8_t servoPin)
{
    motor = new ServoMotor(servoPin);
}

RobotArmComponent::~RobotArmComponent()
{
    delete motor;
}

void RobotArmComponent::update()
{
    motor->update();
}

ServoMotor *RobotArmComponent::getMotor()
{
    return motor;
}

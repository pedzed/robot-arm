#include <stdint.h>
#include "Instruction.h"

using PedZed::RobotArm::Instruction;

Instruction::Instruction()
    : delayTime(0),
    baseAngle(0),
    upperarmAngle(0),
    forearmAngle(0),
    verticalWristAngle(0),
    horizontalWristAngle(0),
    clawAngle(0)
{
    //
}

Instruction::Instruction(
    uint16_t delayTime,
    int16_t baseAngle,
    int16_t upperarmAngle,
    int16_t forearmAngle,
    int16_t verticalWristAngle,
    int16_t horizontalWristAngle,
    int16_t clawAngle
) : delayTime(delayTime),
    baseAngle(baseAngle),
    upperarmAngle(upperarmAngle),
    forearmAngle(forearmAngle),
    verticalWristAngle(verticalWristAngle),
    horizontalWristAngle(horizontalWristAngle),
    clawAngle(clawAngle)
{
    //
}

uint16_t Instruction::getDelayTime()
{
    return delayTime;
}

int16_t Instruction::getBaseAngle()
{
    return baseAngle;
}

int16_t Instruction::getUpperarmAngle()
{
    return upperarmAngle;
}

int16_t Instruction::getForearmAngle()
{
    return forearmAngle;
}

int16_t Instruction::getVerticalWristAngle()
{
    return verticalWristAngle;
}

int16_t Instruction::getHorizontalWristAngle()
{
    return horizontalWristAngle;
}

int16_t Instruction::getClawAngle()
{
    return clawAngle;
}

#include <Arduino.h>
#include <cantino.h>
#include "RobotArm.h"

using namespace cantino;
using PedZed::RobotArm::RobotArm;

RobotArm::RobotArm()
{
    setupBase();
    setupArm();
    setupWrist();
    setupClaw();

    instructionFinishedTime = NULL;
}

void RobotArm::setupBase()
{
    base = new RobotArmComponent(8);
    base->getMotor()->setPulseWidthRange(500, 2500);
    base->getMotor()->setInversion(true);
    base->getMotor()->setSpeed(1);
}

void RobotArm::setupArm()
{
    upperarm = new RobotArmComponent(9);
    upperarm->getMotor()->setPulseWidthRange(500, 2500);
    upperarm->getMotor()->setInversion(false);
    upperarm->getMotor()->setWriteIntervalTime(40);
    upperarm->getMotor()->setSpeed(1);

    forearm = new RobotArmComponent(10);
    forearm->getMotor()->setPulseWidthRange(500, 2500);
    forearm->getMotor()->setInversion(true);
    forearm->getMotor()->setSpeed(1);
}

void RobotArm::setupWrist()
{
    verticalWrist = new RobotArmComponent(11);
    verticalWrist->getMotor()->setPulseWidthRange(500, 2500);
    verticalWrist->getMotor()->setInversion(true);
    verticalWrist->getMotor()->setSpeed(1);

    horizontalWrist = new RobotArmComponent(12);
    horizontalWrist->getMotor()->setPulseWidthRange(500, 2500);
    horizontalWrist->getMotor()->setInversion(false);
    horizontalWrist->getMotor()->setSpeed(1);
}

void RobotArm::setupClaw()
{
    claw = new RobotArmComponent(13);
    claw->getMotor()->setPulseWidthRange(500, 2500);
    claw->getMotor()->setInversion(false);
    claw->getMotor()->setSpeed(1);
}

RobotArm::~RobotArm()
{
    delete base;
    delete upperarm;
    delete forearm;
    delete verticalWrist;
    delete horizontalWrist;
    delete claw;
}

void RobotArm::update()
{
    if (!hasInstructions()) {
        return;
    }

    Instruction instruction = instructions->peek();

    passInstructionToMotors(instruction);
    updateComponents();

    if (!areMotorsFinishedRotating()) {
        return;
    }

    bool isInstructionPopped = popInstructionAfterDelay(instruction.getDelayTime());

    if (!isInstructionPopped) {
        return;
    }

    cout << "[RobotArm] ";
    cout << "Instructions left: " << instructions->count();
    cout << " -- ";
    cout << "Motor angles: ";
    cout << "Base: " << base->getMotor()->currentAngle();
    cout << " | Upperarm: " << upperarm->getMotor()->currentAngle();
    cout << " | Forearm: " << forearm->getMotor()->currentAngle();
    cout << " | V Wrist: " << verticalWrist->getMotor()->currentAngle();
    cout << " | H Wrist: " << horizontalWrist->getMotor()->currentAngle();
    cout << " | Claw: " << claw->getMotor()->currentAngle();
    cout << endl;
}

void RobotArm::passInstructionToMotors(Instruction instruction)
{
    base->getMotor()->turnToAngle(instruction.getBaseAngle());
    upperarm->getMotor()->turnToAngle(instruction.getUpperarmAngle());
    forearm->getMotor()->turnToAngle(instruction.getForearmAngle());
    verticalWrist->getMotor()->turnToAngle(instruction.getVerticalWristAngle());
    horizontalWrist->getMotor()->turnToAngle(instruction.getHorizontalWristAngle());
    claw->getMotor()->turnToAngle(instruction.getClawAngle());
}

void RobotArm::updateComponents()
{
    base->update();
    upperarm->update();
    forearm->update();
    verticalWrist->update();
    horizontalWrist->update();
    claw->update();
}

bool RobotArm::areMotorsFinishedRotating()
{
    return (
        base->getMotor()->isRotationFinished() &&
        upperarm->getMotor()->isRotationFinished() &&
        forearm->getMotor()->isRotationFinished() &&
        verticalWrist->getMotor()->isRotationFinished() &&
        horizontalWrist->getMotor()->isRotationFinished() &&
        claw->getMotor()->isRotationFinished()
    );
}

bool RobotArm::popInstructionAfterDelay(uint32_t delayTime)
{
    uint32_t currentTime = millis();

    if (instructionFinishedTime == NULL) {
        instructionFinishedTime = currentTime;
    }

    if (currentTime - instructionFinishedTime < delayTime) {
        return false;
    }

    instructions->pop();
    instructionFinishedTime = NULL;

    return true;
}

void RobotArm::setInstructions(Queue<Instruction> *instructions)
{
    this->instructions = instructions;
}

void RobotArm::unsetInstructions()
{
    instructions->clear();
}

bool RobotArm::hasInstructions()
{
    return (instructions->count() > 0);
}

void RobotArm::enable()
{
    base->getMotor()->enable();
    upperarm->getMotor()->enable();
    forearm->getMotor()->enable();
    verticalWrist->getMotor()->enable();
    horizontalWrist->getMotor()->enable();
    claw->getMotor()->enable();
}

void RobotArm::disable()
{
    base->getMotor()->disable();
    upperarm->getMotor()->disable();
    forearm->getMotor()->disable();
    verticalWrist->getMotor()->disable();
    horizontalWrist->getMotor()->disable();
    claw->getMotor()->disable();
}

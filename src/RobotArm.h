#pragma once

#include "Instruction.h"
#include "Queue.h"
#include "RobotArmComponent.h"

namespace PedZed { namespace RobotArm
{
    class RobotArm
    {
        public:
            RobotArm();
            ~RobotArm();

            void update();

            void setInstructions(Queue<Instruction> *);
            void unsetInstructions();
            bool hasInstructions();

            void enable();
            void disable();

        private:
            RobotArmComponent *base;
            RobotArmComponent *upperarm;
            RobotArmComponent *forearm;
            RobotArmComponent *verticalWrist;
            RobotArmComponent *horizontalWrist;
            RobotArmComponent *claw;

            Queue<Instruction> *instructions;

            uint32_t instructionFinishedTime;

            void setupBase();
            void setupArm();
            void setupWrist();
            void setupClaw();

            void passInstructionToMotors(Instruction);
            void updateComponents();
            bool areMotorsFinishedRotating();
            bool popInstructionAfterDelay(uint32_t delayTime);
    };
}}

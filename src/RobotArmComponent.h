#pragma once

#include "ServoMotor.h"

using PedZed::ArduinoUtils::ServoMotor;

namespace PedZed { namespace RobotArm
{
    class RobotArmComponent
    {
        public:
            RobotArmComponent(uint8_t openClosePin);
            ~RobotArmComponent();

            void update();
            ServoMotor *getMotor();

        private:
            ServoMotor *motor;
    };
}}

#pragma once

namespace PedZed { namespace RobotArm
{
    class Instruction
    {
        public:
            Instruction();
            Instruction(
                uint16_t delayTime,
                int16_t baseAngle,
                int16_t upperarmAngle,
                int16_t forearmAngle,
                int16_t verticalWristAngle,
                int16_t horizontalWristAngle,
                int16_t clawAngle
            );

            uint16_t getDelayTime();
            int16_t getBaseAngle();
            int16_t getUpperarmAngle();
            int16_t getForearmAngle();
            int16_t getVerticalWristAngle();
            int16_t getHorizontalWristAngle();
            int16_t getClawAngle();

        private:
            uint16_t delayTime;
            int16_t baseAngle;
            int16_t upperarmAngle;
            int16_t forearmAngle;
            int16_t verticalWristAngle;
            int16_t horizontalWristAngle;
            int16_t clawAngle;
    };
}}

# Robot Arm

_Unarming your workload._

![Robot Arm](/docs/images/robot-arm-1.jpg)

![Robot Arm](/docs/images/robot-arm-2.jpg)


## Installation

Check the `/docs` directory.


## Codebase

The code is split into general Arduino utilities (`PedZed::ArduinoUtils`) like the PushButton class, and specific robot arm code (`PedZed::RobotArm`).


## To-do's

- Create a GUI for writing instruction sets
